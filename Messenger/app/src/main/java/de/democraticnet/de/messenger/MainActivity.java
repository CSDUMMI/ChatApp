package de.democraticnet.de.messenger;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_CIPHER_CHOICE = "de.democraticnet.de.CIPHER_CHOICE";

    public static String EXTRA_CHAT_ID = "de.democraticnet.de.CHAT_ID";

    CipherAlgorithm cipherAlgorithmChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        cipherAlgorithmChoice = CipherAlgorithm.AES256GCMNOPADDING;
    }


    /**
     * Open the chat activity on the given chat id.
     *
     * @param view
     * @throws IOException
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     */
    public void openChat(View view) {

        Intent intent = new Intent(this, ChatActivity.class);

        EditText chatIdText = (EditText) findViewById(R.id.chat_id_text);

        String chatId = chatIdText.getText().toString().replace('§', '$');

        intent.putExtra(MainActivity.EXTRA_CHAT_ID, chatId);

        intent.putExtra(MainActivity.EXTRA_CIPHER_CHOICE, cipherAlgorithmChoice.getNumber());

        startActivity(intent);

    }


    public void choose(View view) {

        RadioButton button = (RadioButton) view;

        if(button.isChecked()) {

            return;

        }

        switch(view.getId()) {

            case R.id.AES256GCMNOPADDING:

                cipherAlgorithmChoice = CipherAlgorithm.AES256GCMNOPADDING;

                break;

            case R.id.CHACHA20NOPADDING:

                cipherAlgorithmChoice = CipherAlgorithm.CHACHA20NOPADDING;

                break;

        }

    }
}