package de.democraticnet.de.messenger;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ChatActivity extends AppCompatActivity {

    String chatId;

    Chat chat;

    CipherAlgorithm cipherAlgorithmChoice;

    Thread thread;

    Thread reloadThread;

    /**
     * If the chat id is found in the key store,
     * then the chat activity is opened right away.
     *
     * If the chat id is not found in the key store and the chat ID is empty on the server,
     * then a KeyAgreement message is sent into the chat at the chat id.
     *
     * If the chat id is not found and the chat ID is not empty on the server,
     * then it is checked if the chat only contains a single key agreement message,
     * that is read, a doPhase called and the resulting message put into the chat, shared secret and chat activity opened.
     *
     * If the chat exists on the server but the KeyAgreement has concluded, the Chat Activity is not opened.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        System.out.println("Providers: " + Arrays.toString(Security.getProviders()));

        Intent intent = getIntent();

        chatId = intent.getStringExtra(MainActivity.EXTRA_CHAT_ID);

        cipherAlgorithmChoice = CipherAlgorithm.valueOf(intent.getIntExtra(MainActivity.EXTRA_CIPHER_CHOICE, CipherAlgorithm.AES256GCMNOPADDING_VALUE));

        createChatInstance();


    }

    @Override
    protected void onDestroy() {

        this.reloadThread.interrupt();

        this.thread.interrupt();

        super.onDestroy();


    }

    @Override
    protected void onStart() {

        super.onStart();

        createReloadThread();


    }

    @Override
    protected void onStop() {

        this.reloadThread.interrupt();

        super.onStop();


    }

    public void createChatInstance() {

        System.out.println("createChatInstance called");

        if(thread != null) {

            return;

        }

        thread = new Thread(this::run);
        
        thread.start();

    }

    public void createReloadThread() {

        TextView output = findViewById(R.id.chat_text_view);

        reloadThread = new Thread(() -> {

            while(true) {

                if(chat != null) {

                    output.setText(chat.toString());

                } else {

                    output.setText("Initializing...");

                }

                try {
                    Thread.sleep(500);

                } catch (InterruptedException e) {

                    break;

                }
            }

        });

        reloadThread.start();

    }

    public void sendMessage(View view)   {

        System.out.println("sendMessage called");
        
        EditText inputField = (EditText) findViewById(R.id.input);

        String inputText = inputField.getText().toString();

        TextView output = (TextView) findViewById(R.id.chat_text_view);

        if(thread != null || !thread.isAlive()) {

            return;

        }

        if(chat == null) {


            createChatInstance();

            return;

        }

        thread = new Thread(() -> {
            try {

                chat.publish(inputText);

            } catch (NoSuchPaddingException | IllegalBlockSizeException | InvalidParameterSpecException | NoSuchAlgorithmException | BadPaddingException | IOException | UnrecoverableEntryException | KeyStoreException | InvalidKeyException | InterruptedException | ExecutionException | NoSuchProviderException e) {

                e.printStackTrace();

            }
        });

        thread.start();

        inputField.setText("");
    }

    private void run() {

        KeyStore keystore = null;

        try {


            keystore = KeyStore.getInstance("AndroidKeyStore");

            keystore.load(null);

        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException e) {

            e.printStackTrace();

            finish();

        }


        if (keystore == null) {

            finish();

        }

        try {

            chat = new Chat(chatId, keystore, getPreferences(Context.MODE_PRIVATE), cipherAlgorithmChoice);

        } catch (IOException | NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | UnrecoverableEntryException | InvalidAlgorithmParameterException | InvalidKeyException | InterruptedException | ExecutionException | InvalidKeySpecException | NoSuchProviderException | KeyStoreException e) {

            e.printStackTrace();

            finish();
        }

        while(true) {
            try {
                Thread.sleep(10000);

            } catch(InterruptedException e) {


                break;
            }
        }
    }
}