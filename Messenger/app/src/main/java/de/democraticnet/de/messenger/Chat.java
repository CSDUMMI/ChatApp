package de.democraticnet.de.messenger;

import android.content.SharedPreferences;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeyProtection;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.AlgorithmParameterGenerator;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHGenParameterSpec;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.GCMParameterSpec;
import javax.net.ssl.HttpsURLConnection;

/**
 * Class for dealing with chats and the chat server
 */
public class Chat {

    static final String CHAT_SERVER_URL = "https://democraticnet.de/";

    boolean isParticipant;

    public String chatId;

    KeyStore keystore;

    SharedPreferences sharedPreferences;

    public CipherAlgorithm cipherAlgorithm;

    State state;

    Message[] messages;

    int offset;

    int length;

    InitChatMsg initChatMsg;

    AcceptChatMsg acceptChatMsg;

    byte[] secretKeyDigest;

    enum State {

        EMPTY,

        KEYAGREEMENT,

        INCOMPLETE,

        OPEN,

        CLOSED;


    }

    public Chat(String _chatId, KeyStore _keystore, SharedPreferences _sharedPreferences, CipherAlgorithm cipherAlgorithmChoice) throws IOException, KeyStoreException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, UnrecoverableEntryException, InvalidAlgorithmParameterException, InvalidKeyException, InterruptedException, ExecutionException, InvalidKeySpecException, NoSuchProviderException {

        chatId = _chatId;

        keystore = _keystore;

        sharedPreferences = _sharedPreferences;

        state = assessState();

        cipherAlgorithm = assessCipherAlgorithm();

        if(cipherAlgorithm == null) {

            cipherAlgorithm = cipherAlgorithmChoice;

        }

        switch(state) {

            case EMPTY:

                init();

                break;

            case KEYAGREEMENT:

                if(!isParticipant) {

                    accept();

                }

                break;

            case INCOMPLETE:

                complete();

                break;

            default:

                break;

        }

        offset = 0;

        length = 20;

        messages = getMessages(offset, length);
    }

    // UI

    public String toString() {

        String digest = "not yet calculated";

        if(secretKeyDigest != null) {

            digest = Base64.encodeToString(secretKeyDigest, Base64.NO_PADDING | Base64.NO_WRAP | Base64.URL_SAFE);

        }

        String header = "Chat ID:\t" + chatId +
                            "\nHash:\t" + digest +
                            "\nState:\t" + state.toString();


        StringBuilder result = new StringBuilder(header);

        if(messages != null) {

            for(Message message: messages) {

                try {

                    result.append(messageToString(message));

                } catch (NoSuchAlgorithmException e) {

                    e.printStackTrace();

                    result.append("ERROR decoding message");

                }

            }

        }



        return result.toString();

    }

    public String messageToString(Message message) throws NoSuchAlgorithmException {

        Boolean authorship = sharedPreferences.getBoolean(Base64.encodeToString(digestMessage(message), Base64.URL_SAFE), false);

        String author = authorship ? "me" : "them";

        author = sharedPreferences.getString(chatId + "§msg§" + author, "me");

        String time = Long.toString(message.getTime());

        return author + " [" + time + "]> "  + message.getContent();
    }

    // Key Agreement and Initialization

    /**
     * Send InitChatMessage if the state is correct.
     *
     * @return true if successfully sent the message.
     */
    public boolean init() throws IOException, ExecutionException, InterruptedException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchProviderException {

        if(state != State.EMPTY) {

            return false;

        }

        KeyPair keyPair = generateDHKeyPair(null);

        sharedPreferences.edit().putString(chatId + "§private", Base64.encodeToString(keyPair.getPrivate().getEncoded(), Base64.URL_SAFE)).apply();

        InitChatMsg msg = InitChatMsg.newBuilder().setAlgorithm(cipherAlgorithm).setKey(ByteString.copyFrom(keyPair.getPublic().getEncoded())).build();

        initChatMsg = msg;

        isParticipant = post(msg);

        if(isParticipant) {

            state = State.KEYAGREEMENT;

        }

        return isParticipant;
    }

    /**
     * Send AcceptChatMessage
     * @return true if successful
     */
    public boolean accept() throws NoSuchAlgorithmException, IOException, ExecutionException, InterruptedException, InvalidKeySpecException, InvalidKeyException, KeyStoreException, InvalidAlgorithmParameterException, NoSuchProviderException {

        if(state != State.KEYAGREEMENT || isParticipant) { // isParticipant: this chat has been initialized by this device.

            return false;

        }

        PublicKey otherPublicKey = decodePublicKey(initChatMsg.getKey().toByteArray());

        KeyPair ownKeyPair = generateDHKeyPair(((DHPublicKey) otherPublicKey).getParams());

        KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");

        keyAgreement.init(ownKeyPair.getPrivate());

        keyAgreement.doPhase(otherPublicKey, true);

        SecretKey key = keyAgreement.generateSecret(getAlgorithmStringSimple());

        secretKeyDigest = setSecretKeyEntry(key);

        AcceptChatMsg msg = AcceptChatMsg.newBuilder().setKey(ByteString.copyFrom(ownKeyPair.getPublic().getEncoded())).build();

        acceptChatMsg = msg;

        if(post(msg)) {

            state = State.OPEN;

            return true;

        } else {

            return false;
        }
    }

    /**
     *
     * @return
     */
    public boolean complete() throws NoSuchAlgorithmException, InvalidKeyException, KeyStoreException, InvalidKeySpecException {

        if(state != State.KEYAGREEMENT || !isParticipant || !sharedPreferences.contains(chatId + "§private") || this.acceptChatMsg == null) {

            return false;

        }
        
        PublicKey otherPublicKey = decodePublicKey(acceptChatMsg.getKey().toByteArray());

        PrivateKey ownPrivateKey = decodePrivateKey(Base64.decode(sharedPreferences.getString(chatId + "§private", ""), Base64.URL_SAFE));

        KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");

        keyAgreement.init(ownPrivateKey);

        keyAgreement.doPhase(otherPublicKey, true);

        SecretKey key = keyAgreement.generateSecret(getAlgorithmStringSimple());

        secretKeyDigest = setSecretKeyEntry(key);

        state = State.OPEN;

        return true;
    }

    private String getAlgorithmStringSimple() {

        switch (cipherAlgorithm) {


            case CHACHA20NOPADDING:

                return "ChaCha20";

            case AES256GCMNOPADDING:

            default:

                return "AES";

        }

    }

    private byte[] setSecretKeyEntry(SecretKey key) throws KeyStoreException, NoSuchAlgorithmException {

        keystore.setEntry(chatId, new KeyStore.SecretKeyEntry(key), (new KeyProtection.Builder(KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_ENCRYPT)).build());

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(key.getEncoded());

        return md.digest();

    }
    

    // predicates

    public boolean canChat() {

        return state == State.OPEN && isParticipant;

    }

    public SecretKey getKey() throws UnrecoverableEntryException, KeyStoreException, NoSuchAlgorithmException {

        if(!canChat()) {

            return null;

        }

        KeyStore.Entry entry = keystore.getEntry(chatId,null);

        if(entry instanceof KeyStore.SecretKeyEntry) {

            return ((KeyStore.SecretKeyEntry) entry).getSecretKey();

        }

        return null;
    }

    // publish and fetch messages

    public boolean publish(String message) throws NoSuchPaddingException, IllegalBlockSizeException, InvalidParameterSpecException, NoSuchAlgorithmException, BadPaddingException, IOException, UnrecoverableEntryException, KeyStoreException, InvalidKeyException, InterruptedException, ExecutionException, NoSuchProviderException {

        if(!canChat()) {

            return false;

        }

        Message msg = Message.newBuilder()
                .setContent(message)
                .setTime(Instant.now().getEpochSecond())
                .build();

        EncryptedMessage encryptedMessage = encryptMessage(msg);


        return post(encryptedMessage);

    }

    public Message[] getMessages(int offset, int length) throws IOException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, UnrecoverableEntryException, KeyStoreException, InvalidAlgorithmParameterException, InvalidKeyException, InterruptedException, ExecutionException, NoSuchProviderException {

        if(!canChat()) {

            return null;

        }

        int start = offset;

        ArrayList<Message> result = new ArrayList<Message>();

        while(result.size() < length) {

            ChatEntry[] entries = get(start, start + length);

            if(entries == null || entries.length == 0) {

                break;

            }

            for(ChatEntry entry : entries) {

                if(entry.hasEncryptedMessage()) {

                    try {

                        Message msg = decryptMessage(entry.getEncryptedMessage());

                        result.add(msg);

                    } catch(NoSuchPaddingException |NoSuchProviderException| NoSuchAlgorithmException |IllegalBlockSizeException| BadPaddingException| InvalidProtocolBufferException| UnrecoverableEntryException| KeyStoreException| InvalidAlgorithmParameterException| InvalidKeyException e) {

                        continue;

                    }
                }

            }

            start += length;
        }

        Message[] msgs = new Message[result.size()];

        result.toArray(msgs);

        return msgs;
    }

    EncryptedMessage encryptMessage(Message message) throws InvalidParameterSpecException, NoSuchProviderException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, UnrecoverableEntryException, KeyStoreException, InvalidKeyException {

        String algorithm = getAlgorithmString();

        Cipher cipher = Cipher.getInstance(algorithm, "AndroidKeyStore");

        cipher.init(Cipher.ENCRYPT_MODE, getKey());

        byte[] ciphertext = cipher.doFinal(message.toByteArray());

        byte[] iv = cipher.getIV();

        byte[] digest = digestMessage(message);

        return EncryptedMessage.newBuilder()
                .setAlgorithm(cipherAlgorithm)
                .setCiphertext(ByteString.copyFrom(ciphertext))
                .setIv(ByteString.copyFrom(iv))
                .setCid(ByteString.copyFrom(digest))
                .build();
    }

    Message decryptMessage(@NonNull EncryptedMessage encryptedMessage) throws NoSuchPaddingException, NoSuchProviderException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidProtocolBufferException, UnrecoverableEntryException, KeyStoreException, InvalidAlgorithmParameterException, InvalidKeyException {

        String algorithm = getAlgorithmString();

        Cipher cipher = Cipher.getInstance(algorithm, "AndroidKeyStore");

        GCMParameterSpec spec = new GCMParameterSpec(0, encryptedMessage.getIv().toByteArray());

        cipher.init(Cipher.DECRYPT_MODE, getKey(), spec);

        byte[] plaintext = cipher.doFinal(encryptedMessage.getCiphertext().toByteArray());

        return Message.parseFrom(plaintext);

    }

    byte[] digestMessage(@NonNull Message message) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(message.toByteArray());

        return md.digest();

    }

    PublicKey decodePublicKey(byte[] encoded) throws NoSuchAlgorithmException, InvalidKeySpecException {

        return KeyFactory.getInstance("DH").generatePublic(new X509EncodedKeySpec(encoded));

    }

    PrivateKey decodePrivateKey(byte[] encoded) throws NoSuchAlgorithmException, InvalidKeySpecException {

        return KeyFactory.getInstance("DH").generatePrivate(new PKCS8EncodedKeySpec(encoded));

    }

    KeyPair generateDHKeyPair(DHParameterSpec spec) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DH");

        if(spec == null) {

            kpg.initialize(2048);

        } else {

            kpg.initialize(spec);

        }

        return kpg.generateKeyPair();

    }

    String getAlgorithmString() {

        String algorithm = "AES/GCM/NoPadding";

        switch (cipherAlgorithm) {

            case CHACHA20NOPADDING:

                algorithm = "ChaCha20/Poly1305/NoPadding";

                break;

            default:

                break;
        }

        return algorithm;

    }


    // assessing the st of the chat

    State assessState() throws IOException, KeyStoreException, InterruptedException, ExecutionException {

        if(keystore.containsAlias(chatId)) {

            cipherAlgorithm = CipherAlgorithm.forNumber(sharedPreferences.getInt(chatId + "§cipherAlgorithm", CipherAlgorithm.AES256GCMNOPADDING_VALUE));

            isParticipant = true;

            return State.OPEN;

        }

        isParticipant = false;

        int i = 0;

        while(true) {

            ChatEntry[] entries = get(i, i + 1);

            if (entries == null || Arrays.equals(entries, new ChatEntry[0])) {

                return State.EMPTY;

            }

            if(entries[0].hasAcceptChatMsg()) {
                
                isParticipant = sharedPreferences.contains(chatId + "§private");

                cipherAlgorithm = CipherAlgorithm.forNumber(sharedPreferences.getInt(chatId + "§cipherAlgorithm", CipherAlgorithm.AES256GCMNOPADDING_VALUE));

                this.acceptChatMsg = entries[0].getAcceptChatMsg();
                
                return isParticipant ? State.INCOMPLETE : State.CLOSED;

            }


            if(entries[0].hasInitChatMsg()) {

                isParticipant = sharedPreferences.contains(chatId + "§private");

                this.initChatMsg = entries[0].getInitChatMsg();

                cipherAlgorithm = CipherAlgorithm.forNumber(sharedPreferences.getInt(chatId + "§cipherAlgorithm", CipherAlgorithm.AES256GCMNOPADDING_VALUE));

                return State.KEYAGREEMENT;

            }

            i++;
        }
    }


    private CipherAlgorithm assessCipherAlgorithm() throws IOException, InterruptedException, ExecutionException {

        if(initChatMsg != null && initChatMsg.getAlgorithm() != null) {

            return initChatMsg.getAlgorithm();

        }

        return null;
    }


    // basic network operations

    boolean post(EncryptedMessage msg) throws IOException, InterruptedException, ExecutionException {

        return postBinary(ChatEntry.newBuilder()
                .setEncryptedMessage(msg)
                .build()
                .toByteArray());

    }

    boolean post(InitChatMsg msg) throws IOException, InterruptedException, ExecutionException {

        return postBinary(ChatEntry.newBuilder()
                .setInitChatMsg(msg)
                .build()
                .toByteArray());

    }

    boolean post(AcceptChatMsg msg) throws IOException, InterruptedException, ExecutionException {

        return postBinary(ChatEntry.newBuilder()
                .setAcceptChatMsg(msg)
                .build()
                .toByteArray());

    }

    boolean postBinary(byte[] data) throws IOException, InterruptedException, ExecutionException {

        String params = "chat_id=" + chatId + "&content=" + Base64.encodeToString(data, Base64.URL_SAFE);

        URL url = new URL(CHAT_SERVER_URL);

        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        connection.setRequestMethod("POST");

        connection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());

        wr.writeBytes(params);

        wr.flush();

        wr.close();

        return connection.getResponseCode() == 200;

    }


    ChatEntry[] get(int start, int end) throws IOException, InterruptedException, ExecutionException {

        String params = "?chat_id=" + chatId +"&start=" + start + "&end=" + end;

        URL url = new URL(CHAT_SERVER_URL + params);

        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        connection.connect();

        if(connection.getResponseCode() != 200) {

            return null;

        }


        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        StringBuilder contentBuilder = new StringBuilder();

        String line;

        while((line = in.readLine()) != null) {

            contentBuilder.append(line);

        }

        in.close();

        String content = contentBuilder.toString();

        ArrayList<String> data = new ArrayList<String>(Arrays.asList(content.replace("[", " ").replace("]", " ").trim().split(",")));

        for(int i = 0; i < data.size(); i++) {

            if(data.get(i) == "") {

                data.remove(i);

            }
        }

        ArrayList<ChatEntry> entries = new ArrayList<ChatEntry>();

        for(String entry : data) {

            entries.add(ChatEntry.parseFrom(Base64.decode(entry, Base64.URL_SAFE)));

        }

        ChatEntry[] result = new ChatEntry[entries.size()];

        entries.toArray(result);

        return result;
    }
}
