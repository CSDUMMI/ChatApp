// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Message.proto

package de.democraticnet.de.messenger;

public interface ChatEntryOrBuilder extends
    // @@protoc_insertion_point(interface_extends:ChatEntry)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.EncryptedMessage encryptedMessage = 2;</code>
   * @return Whether the encryptedMessage field is set.
   */
  boolean hasEncryptedMessage();
  /**
   * <code>.EncryptedMessage encryptedMessage = 2;</code>
   * @return The encryptedMessage.
   */
  de.democraticnet.de.messenger.EncryptedMessage getEncryptedMessage();
  /**
   * <code>.EncryptedMessage encryptedMessage = 2;</code>
   */
  de.democraticnet.de.messenger.EncryptedMessageOrBuilder getEncryptedMessageOrBuilder();

  /**
   * <code>.InitChatMsg initChatMsg = 3;</code>
   * @return Whether the initChatMsg field is set.
   */
  boolean hasInitChatMsg();
  /**
   * <code>.InitChatMsg initChatMsg = 3;</code>
   * @return The initChatMsg.
   */
  de.democraticnet.de.messenger.InitChatMsg getInitChatMsg();
  /**
   * <code>.InitChatMsg initChatMsg = 3;</code>
   */
  de.democraticnet.de.messenger.InitChatMsgOrBuilder getInitChatMsgOrBuilder();

  /**
   * <code>.AcceptChatMsg acceptChatMsg = 4;</code>
   * @return Whether the acceptChatMsg field is set.
   */
  boolean hasAcceptChatMsg();
  /**
   * <code>.AcceptChatMsg acceptChatMsg = 4;</code>
   * @return The acceptChatMsg.
   */
  de.democraticnet.de.messenger.AcceptChatMsg getAcceptChatMsg();
  /**
   * <code>.AcceptChatMsg acceptChatMsg = 4;</code>
   */
  de.democraticnet.de.messenger.AcceptChatMsgOrBuilder getAcceptChatMsgOrBuilder();

  public de.democraticnet.de.messenger.ChatEntry.DataCase getDataCase();
}
