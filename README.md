# Chat App

## Sichere Kommunikation
Die Anforderungen der Aufgabenstellungen geben zwei Anforderungen an die App an:

> [1.] Wahl eines symmetrsichen Verschlüsselungsverfahrens (aus mindestens 2).
> [2.] Speichern und Laden von Schlüsseln.

Aus der Anforderung 1. folgt diese Grundannahme im Gefahrenmodell:
Eines der Verschlüsselungsverfahren könnte fehlerhaft sein, weshalb mehrere Option
verfügbar sein müssen.

Aus Anforderung 2. folgt, dass eine Integration mit dem Key Store des Betriebssystems gewünscht ist.

Eine Chat Nachricht ist so dargestellt:
```protobuf
message Message {
    bytes cid = 1;

    string text = 2;

    bytes chat_id = 3;
}
```

Der Autor einer Nachricht ist gespeichert, indem ein jeder Teilnehmer die CIDs seiner Nachrichten speichert.

Ein Chat isst so dargestellt:
```protobuf
message Chat {

    repeated Message messages = 1;

    bytes chat_id = 2;

    string participant_name_1 = 3;

}
```
Das own_name und other_name Feld sind die Pseudonyme der Personen.
